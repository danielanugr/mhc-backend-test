const { MongoClient } = require('mongodb')

let database = null

async function connect () {
  try {
    const uri = process.env.MONGO_URI
    const client = new MongoClient(uri, { useUnifiedTopology: true })

    await client.connect()

    const db = client.db(process.env.MONGO_DB_NAME)

    database = db

    return db
  } catch (err) {
    console.log(err)
  }
}

module.exports = {
  connect,
  getDatabase () {
    return database
  }
}
