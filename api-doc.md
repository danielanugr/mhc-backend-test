# MHC test API documentation

**Register**

This is the endpoint for user to register an account.

* **URL**

  /register

* **Method**

  `POST`

* **Data Constraint**

  ```
  {
    "name" : <string>
    "email" : <string>
    "password: <string>
  }
  ```

* **Success Response**

  **Code :** `200 OK`
  ```
  {
    "id" : <objectId>
    "name" : <string>
    "email" : <string>
    "password: <string>
    "access_token": <string>
  }
  ```

* **Error Response**

  **Condition :** If email is already registered
  
  **Code :** `400 Bad Request`
  
  ```
  {
    "message": Email already registered
  }
  ``` 

**Login**

This is the endpoint for login process.

* **URL**

  /login

* **Method**

  `POST`

* **Data Constraint**

  ```
  {
    "email" : <string>
    "password: <string>
  }
  ```

* **Success Response**

  **Code :** `200 OK`
  ```
  {
    "access_token": <string>
  }
  ```

* **Error Response**

  **Condition :** If the user input wrong email/password

  **Code :** `400 Bad Request`
  
  ```
  {
    "message": Email already registered
  }
  ``` 

**Crypto Currency List**

This is the endpoint for login process.

* **URL**

  /crypto

* **Method**

  `Get`

* **Headers**

  ```
  {
    "access_token": <string>
  }
  ```

* **Success Response**

  **Code :** `200 OK`
  ```
    [
      {
        "id": 1,
        "name": "Bitcoin",
        "symbol": "BTC",
        "slug": "bitcoin",
        "num_market_pairs": 9547,
        "date_added": "2013-04-28T00:00:00.000Z",
        "tags": [
            "mineable",
            "pow",
            "sha-256",
            "store-of-value",
            "state-channels",
            "coinbase-ventures-portfolio",
            "three-arrows-capital-portfolio",
            "polychain-capital-portfolio",
            "binance-labs-portfolio",
            "arrington-xrp-capital",
            "blockchain-capital-portfolio",
            "boostvc-portfolio",
            "cms-holdings-portfolio",
            "dcg-portfolio",
            "dragonfly-capital-portfolio",
            "electric-capital-portfolio",
            "fabric-ventures-portfolio",
            "framework-ventures",
            "galaxy-digital-portfolio",
            "huobi-capital",
            "alameda-research-portfolio",
            "a16z-portfolio",
            "1confirmation-portfolio",
            "winklevoss-capital",
            "usv-portfolio",
            "placeholder-ventures-portfolio",
            "pantera-capital-portfolio",
            "multicoin-capital-portfolio",
            "paradigm-xzy-screener"
        ],
        "max_supply": 21000000,
        "circulating_supply": 18692562,
        "total_supply": 18692562,
        "platform": null,
        "cmc_rank": 1,
        "last_updated": "2021-04-27T10:13:02.000Z",
        "quote": {
            "USD": {
                "price": 54527.89669132662,
                "volume_24h": 51828775334.43301,
                "percent_change_1h": -0.4384569,
                "percent_change_24h": 3.8302634,
                "percent_change_7d": -1.03912055,
                "percent_change_30d": -3.67066783,
                "percent_change_60d": 17.53091197,
                "percent_change_90d": 75.52790915,
                "market_cap": 1019266089632.2177,
                "last_updated": "2021-04-27T10:13:02.000Z"
            }
        }
      }
    ]
  ```

* **Error Response**

  **Condition :** If the user don't include token

  **Code :** `400 Bad Request`
  
  ```
  {
    "message": Invalid Token
  }
  ``` 