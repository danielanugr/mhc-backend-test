const { getDatabase } = require('../configs/mongodb')

class User {
  static find (user) {
    return getDatabase()
      .collection('Users')
      .findOne({ email: user })
  }

  static register (user) {
    return getDatabase()
      .collection('Users')
      .insertOne(user)
  }
}

module.exports = User
