const { verifyToken } = require('../helpers/jwt')

function authenticate (req, res, next) {
  try {
    const decoded = verifyToken(req.headers.access_token) //check either the access token is valid or not, and save the decoded information in a variable
    req.decoded = decoded //put the information in custom header named decoded
    next()
  } catch (err) {
    res.status(401).json({ message: 'invalid token' }) //if the access token provided is invalid, it will return error message with status code 401
  }
}

module.exports = authenticate
