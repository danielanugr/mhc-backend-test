if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config()
}

const express = require('express')
const app = express()
const PORT = process.env.PORT || 3000
const router = require('./routes')
const cors = require('cors')
const { connect } = require('./configs/mongodb')

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(router)

connect().then(async db => {
  console.log('mongo connected')

  app.listen(PORT, () => {
    console.log(`App running at port ${PORT}`)
  })
})
