## Setup

For first time use, you need to install all the package needed using `npm install` in your console/terminal.

After that, you need to make a new file named **.env** and input this field:
  
  ```
  JWT_SECRET=<any secret key>
  MONGO_URI=<your mongodb uri>
  MONGO_DB_NAME=<your database name>
  COIN_MARKET_API_KEY=<your coinmarket API key>
  ```
You could get your mongodb URI from mongoDB Compass

---
## Run the server

Run the server using `npx nodemon` command in console/terminal, it will run on [localhost:3000](http://localhost:3000)