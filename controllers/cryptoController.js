const axios = require('axios')

module.exports = class CryptoController {
  static getCrypto (req, res) {
    axios
      .get(
        'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest',
        {
          headers: {
            'X-CMC_PRO_API_KEY': process.env.COIN_MARKET_API_KEY //the 3rd party API require custom headers named 'X-CMC_PRO_API_KEY'
          }
        }
      )
      .then(response => {
        res.status(200).json(response.data.data) //send status code 200 with the crypto currency list
      })
      .catch(err => {
        res.status(500).json({ message: 'internal server error' }) //send an error with status code 500
      })
  }
}
