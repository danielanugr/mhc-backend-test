const User = require('../models/user')
const { hashPassword, comparePassword } = require('../helpers/bcrypt')
const { generateToken } = require('../helpers/jwt')

module.exports = class UserController {
  static async register (req, res) {
    try {
      let error = []
      for (let key in req.body) {
        if (req.body[key] === '') {
          error.push(`${key} cannot be empty`) //check if any filed is empty
        }
      }
      if (error.length > 0) {
        //if there's empty field, throw an error
        throw error
      }
      let { name, email, password } = req.body
      const findUser = await User.find(email) //find the email either it's already registered or not
      if (!findUser) {
        //if the email is not registered, account will be created
        password = hashPassword(password)
        const access_token = await generateToken({ name, email, password }) //hash the password
        const newUser = await User.register({
          name,
          email,
          password,
          access_token
        }) //Add the user data to database
        res.status(201).json(newUser.ops)
      } else {
        res.status(400).json({
          message: 'email already registered'
        }) //if the email is already registered, it will send error message with status code 400
      }
    } catch (err) {
      if (Array.isArray(err)) {
        //if the error comes from line 16, it will send error message with status code 400
        res.status(400).json({ message: err })
      } else {
        res.status(500).json({ message: 'internal server error' }) ////send status code 500 if there's error in the server
      }
    }
  }

  static async login (req, res) {
    try {
      let { email, password } = req.body
      const user = await User.find(email) //find the email in the database
      //check if the user is registered or not
      if (user) {
        const verify = await comparePassword(password, user.password) //verify if the password is correct or not
        if (verify) {
          res.status(200).json({
            name: user.name,
            email: user.email,
            access_token: user.access_token
          }) //send the access token as json
        } else {
          res.status(400).json({ message: 'wrong email/password' }) //send status code 400 with error message if the password doesn't match
        }
      } else {
        res.status(400).json({ message: 'wrong email/password' }) //send status code 400 with error message if the email isn't registered
      }
    } catch (err) {
      res.status(500).json({ message: 'internal server error' }) //send status code 500 if there's error in the server
    }
  }
}
