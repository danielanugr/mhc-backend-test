const jwt = require('jsonwebtoken')

function generateToken (payload) {
  return jwt.sign(payload, process.env.JWT_SECRET) //this will return a token
}

function verifyToken (token) {
  return jwt.verify(token, process.env.JWT_SECRET) //this will cerify and return the data saved in the token
}

module.exports = { generateToken, verifyToken }
