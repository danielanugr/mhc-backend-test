const router = require('express').Router()
const UserController = require('../controllers/userController')
const cryptoRouter = require('./cryptoRoutes')

router.post('/register', UserController.register)
router.post('/login', UserController.login)
router.use('/crypto', cryptoRouter)

module.exports = router
