const router = require('express').Router()
const CryptoController = require('../controllers/cryptoController')
const authenticate = require('../middlewares/authentication')

//authenticate is used so that the enpoint can only accessed if you're logged in
router.get('/', authenticate, CryptoController.getCrypto)

module.exports = router
